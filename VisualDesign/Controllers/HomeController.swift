//
//  HomeController.swift
//  VisualDesign
//
//  Created by Muhammad Alam Akbar on 1/6/17.
//  Copyright © 2017 Muhammad Alam Akbar. All rights reserved.
//

import UIKit

class HomeController: UIViewController {
    
    static func instantiateNav() -> UINavigationController {
        let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeNav") as! UINavigationController
        //let controller = nav.topViewController as! HomeController
        return nav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    @IBAction func eventButtonPressed(_ sender: Any) {
        pushEventController()
    }
    
    @IBAction func guestButtonPressed(_ sender: Any) {
        pushGuestController()
    }
    
    private func pushEventController() {
        let nav = EventController.instantiateNav()
        let controller = nav.topViewController as! EventController
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func pushGuestController() {
        let nav = GuestController.instantiateNav()
        let controller = nav.topViewController as! GuestController
        navigationController?.pushViewController(controller, animated: true)
    }

}
    
