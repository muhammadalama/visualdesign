//
//  ViewController.swift
//  VisualDesign
//
//  Created by Muhammad Alam Akbar on 1/6/17.
//  Copyright © 2017 Muhammad Alam Akbar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var provideProfileLabel: UILabel!
    @IBOutlet weak var addProfileImageVIew: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        welcomeLabel.text = NSLocalizedString("Welcome", comment: "")
        provideProfileLabel.text = NSLocalizedString("Please provide picture", comment: "")
    
        //doneButton.addTarget(self, action: #selector(ViewController.buttonDown(_:)), for: .touchDown)
        //doneButton.addTarget(self, action: #selector(ViewController.buttonUp(_:)), for: .touchUpInside)
        doneButton.layer.cornerRadius = 4
        doneButton.layer.borderWidth = 1
        doneButton.layer.borderColor = UIColor.white.cgColor
    }

    @IBAction func doneButtonPressed(_ sender: Any) {
        var message = "is not palindrome"
        if isPalindrome(nameTextField.text) {
            message = "is palindrome"
        }
        
        let alert = UIAlertController(title: "Info", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {action in
            self.pushHomeController()
        }))
        present(alert, animated: true, completion: nil)
    }
    
    private func pushHomeController() {
        let nav = HomeController.instantiateNav()
        let controller = nav.topViewController as! HomeController
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func isPalindrome(_ text: String!) -> Bool {
        let textNoSpace = text.replacingOccurrences(of: " ", with: "")
        let textReverse = String(textNoSpace.characters.reversed())
        return textNoSpace == textReverse;
    }
    
}
