//
//  EventController.swift
//  VisualDesign
//
//  Created by Muhammad Alam Akbar on 1/6/17.
//  Copyright © 2017 Muhammad Alam Akbar. All rights reserved.
//

import UIKit

class EventController: UIViewController {

    static func instantiateNav() -> UINavigationController {
        let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EventNav") as! UINavigationController
        //let controller = nav.topViewController as! HomeController
        return nav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
